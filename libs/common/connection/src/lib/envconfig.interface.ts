export interface EnvConfig {
  MYSQL_HOST?: string;
  MYSQL_USER?: string;
  MYSQL_PASSWORD?: string;
  MYSQL_DATABASE?: string;
}
