import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions';
import { CommonConnectionService } from './common-connection.service';

export function typeormConnection(config: CommonConnectionService): MysqlConnectionOptions {
  return {
    type: 'mysql',
    host: config.get('MYSQL_HOST'),
    port: 3306,
    username: config.get('MYSQL_USER'),
    password: config.get('MYSQL_PASSWORD'),
    database: config.get('MYSQL_DATABASE'),
    entities: [],
    synchronize: true,
    logging: false,
  };
}
export const DEFAULT = 'default';
