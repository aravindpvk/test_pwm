import { Module } from '@nestjs/common';
import { CommonConnectionService } from './common-connection.service';

@Module({
  controllers: [],
  providers: [CommonConnectionService],
  exports: [CommonConnectionService],
})
export class CommonConnectionModule {}
