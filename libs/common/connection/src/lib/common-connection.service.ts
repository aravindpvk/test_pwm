import { Injectable } from '@nestjs/common';
import * as dotenv from 'dotenv';
import * as Joi from '@hapi/joi';
import { EnvConfig } from './envconfig.interface';
@Injectable()
export class CommonConnectionService {
  private readonly envConfig: EnvConfig;

  constructor() {
    const config = dotenv.config().parsed;
    this.envConfig = this.validateInput(config);
  }

  /**
   * Ensures all needed variables are set, and returns the validated JavaScript object
   * including the applied default values.
   */
  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      MYSQL_DATABASE: Joi.string().required(),
      MYSQL_HOST: Joi.string().required(),
      MYSQL_USER: Joi.string().required(),
      NODE_ENV: Joi.string().required().valid('production', 'development', 'UAT', 'staging'),
      MYSQL_PASSWORD: Joi.string().required(),
    });
    const { error, value: validatedEnvConfig } = envVarsSchema.validate(envConfig);
    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }
    return validatedEnvConfig;
  }

  get(key: string): string {
    return this.envConfig[key];
  }
}
