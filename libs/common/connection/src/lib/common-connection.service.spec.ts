import { Test } from '@nestjs/testing';
import { CommonConnectionService } from './common-connection.service';

describe('CommonConnectionService', () => {
  let service: CommonConnectionService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [CommonConnectionService],
    }).compile();

    service = module.get(CommonConnectionService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
