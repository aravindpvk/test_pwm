import { async, TestBed } from '@angular/core/testing';
import { DigitalOnboardingDashboardModule } from './digital-onboarding-dashboard.module';

describe('DigitalOnboardingDashboardModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [DigitalOnboardingDashboardModule],
    }).compileComponents();
  }));

  // TODO: Add real tests here.
  //
  // NB: This particular test does not do anything useful.
  //     It does NOT check for correct instantiation of the module.
  it('should have a module definition', () => {
    expect(DigitalOnboardingDashboardModule).toBeDefined();
  });
});
