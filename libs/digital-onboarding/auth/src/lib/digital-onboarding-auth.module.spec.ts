import { async, TestBed } from '@angular/core/testing';
import { DigitalOnboardingAuthModule } from './digital-onboarding-auth.module';

describe('DigitalOnboardingAuthModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [DigitalOnboardingAuthModule],
    }).compileComponents();
  }));

  // TODO: Add real tests here.
  //
  // NB: This particular test does not do anything useful.
  //     It does NOT check for correct instantiation of the module.
  it('should have a module definition', () => {
    expect(DigitalOnboardingAuthModule).toBeDefined();
  });
});
