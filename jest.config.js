module.exports = {
  projects: [
    '<rootDir>/apps/digital-onboarding',
    '<rootDir>/apps/digital-onboarding-backend',
    '<rootDir>/libs/digital-onboarding/auth',
    '<rootDir>/libs/digital-onboarding/dashboard',
    '<rootDir>/libs/digital-onboarding/page-not-found',
    '<rootDir>/libs/common/connection',
    '<rootDir>/libs/digital-onboarding/core',
    '<rootDir>/libs/digital-onboarding/shared',
  ],
};
