import { Component } from '@angular/core';

@Component({
  selector: 'pwm-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'digital-onboarding';
}
