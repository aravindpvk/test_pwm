import { Module } from '@nestjs/common';
import { AuthController } from './controller/auth/auth.controller';
import { AuthAggregateService } from './aggregates/auth-aggregate/auth-aggregate.service';

@Module({
  controllers: [AuthController],
  providers: [AuthAggregateService],
})
export class AuthModule {}
