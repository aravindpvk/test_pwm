import { Module } from '@nestjs/common';
import { UserController } from './controller/user.controller';
import { UserService } from './services/user.service';
import { UserAggregateService } from './aggregates/user-aggregate.service';

@Module({
  controllers: [UserController],
  providers: [UserService, UserAggregateService],
  exports:[UserService,UserAggregateService]
})
export class UserModule {}
