import { INestApplication } from '@nestjs/common';
import { readFileSync } from 'fs';
import { join } from 'path';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

export function setupSwagger(app: INestApplication) {
  const version = JSON.parse(readFileSync(join(process.cwd(), 'package.json'), 'utf-8')).version;
  const options = new DocumentBuilder().setTitle('Digital Onboarding').setVersion(version).build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api-docs', app, document);
}
