import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeormConnection, DEFAULT, CommonConnectionModule, CommonConnectionService } from '@pwm/common/connection';
@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      name: DEFAULT,
      imports: [CommonConnectionModule],
      inject: [CommonConnectionService],
      useFactory: typeormConnection,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
